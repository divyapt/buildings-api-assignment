package com.msr;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class BuildingsApiApplication {
	public static void main(String[] args) {
		SpringApplication.run(BuildingsApiApplication.class, args);
	}

}
