package com.msr;

import com.msr.data.*;
import com.msr.model.MaxSqftSizeSiteData;
import com.msr.model.Site;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * Respond to site requests
 *
 * @author Measurabl
 * @since 2019-06-06
 */
@RestController
@RequestMapping("/buildings")
public class BuildingsController {

    @Autowired
    SiteDao siteDaoService;

    /* Route to return a Site by id. Supplement the Site with its use-details:
      a. total_size, by summing the size_sqft associated with the site’s use(s).
      b. primary_type (an object with use_type id and name), where the primary
    */
    @GetMapping("/site/{id}")
    public MaxSqftSizeSiteData getMaxSqftSizeSite(@PathVariable Integer id) {

        return siteDaoService.findMaxSqftSizeSite(id);
    }


    /* Route which returns a list of all sites */
    @GetMapping("/sites")
    public List<Site> getAllSites() {

        return siteDaoService.findAllSites();
    }

    /* Route which returns a list of all sites which have a particular use type */
    @GetMapping("/sitesByUseType/{useTypeId}")
    public Set<Site> getAllSitesForAUseType(@PathVariable Integer useTypeId) {

        return siteDaoService.findAllSitesForAUseType(useTypeId);
    }


    /* Route to create/update a Site */
    @PostMapping("/site")
    public void createUser(@RequestBody Site site) {
        Site savedSite = siteDaoService.saveSite(site);

    }

    /* Route to delete a Site */
    @DeleteMapping("/sites/{id}")
    public void createUser(@PathVariable Integer id) {
        siteDaoService.deleteSiteById(id);

    }
}

////////////////////////////////////////////////////////////
// Copyright 2018  Measurabl, Inc. All rights reserved.
////////////////////////////////////////////////////////////
    