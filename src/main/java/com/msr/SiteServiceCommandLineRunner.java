package com.msr;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.msr.data.SiteDao;
import com.msr.data.SiteRepository;
import com.msr.data.SiteUsesRepository;
import com.msr.data.UseTypesRepository;
import com.msr.model.Site;
import com.msr.model.SiteUses;
import com.msr.model.UseTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Command line runner to insert json data into database
 */
@Component
public class SiteServiceCommandLineRunner implements CommandLineRunner {

    @Autowired
    SiteRepository siteRepository;

    @Autowired
    SiteUsesRepository siteUsesRepository;

    @Autowired
    UseTypesRepository useTypesRepository;

    @Override
    public void run(String... args) throws Exception {
        ObjectMapper mapper = new ObjectMapper();

        //TODO: Move the below repeated code into a utility

        TypeReference<List<Site>> typeReference = new TypeReference<List<Site>>() {
        };
        TypeReference<List<SiteUses>> siteUsesReference = new TypeReference<List<SiteUses>>() {
        };
        TypeReference<List<UseTypes>> useTypeReference = new TypeReference<List<UseTypes>>() {
        };
        // Load Sites into database from json file
        InputStream inputStream = TypeReference.class.getResourceAsStream("/data/sites.json");
        InputStream siteUsesInputStream = TypeReference.class.getResourceAsStream("/data/site_uses.json");
        InputStream useTypesInputSTream = TypeReference.class.getResourceAsStream("/data/use_types.json");
        try {
            List<Site> sites = mapper.readValue(inputStream, typeReference);
            siteRepository.saveAll(sites);
            List<UseTypes> useTypes = mapper.readValue(useTypesInputSTream, useTypeReference);
            useTypesRepository.saveAll(useTypes);
            List<SiteUses> siteUses = mapper.readValue(siteUsesInputStream, siteUsesReference);
            siteUsesRepository.saveAll(siteUses);

            System.out.println("All data saved!");
        } catch (IOException e) {
            System.out.println("Unable to save Sites: " + e.getMessage());
        }
    }
}
