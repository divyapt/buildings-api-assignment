package com.msr.data;

import com.msr.exception.SiteNotFoundException;
import com.msr.model.MaxSqftSizeSiteData;
import com.msr.model.Site;
import com.msr.model.SiteUses;
import com.msr.model.UseTypes;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Global DAO functionality not domain driven
 *
 * @author Measurabl
 * @since 2019-06-06
 */
@Service
@Slf4j
public class SiteDao {
    @Autowired
    private SiteRepository siteRepository;

    @Autowired
    private SiteUsesRepository siteUsesRepository;

    @Autowired
    private UseTypesRepository useTypesRepository;

    /* Get site by Id */
    public Site findSiteById(int id) {

        Site site = siteRepository.findById(id).orElse(null);
        if (site == null) {
            throw new SiteNotFoundException("id- " + id);
        }
        return site;
    }

    /* Return a Site by id. Supplement the Site with its use-details:
       a. total_size, by summing the size_sqft associated with the site’s use(s).
       b. primary_type (an object with use_type id and name), where the primary
     */
    public MaxSqftSizeSiteData findMaxSqftSizeSite(int id) {
        Site site = findSiteById(id);
        List<SiteUses> siteUses = site.getSiteUses();
        // find the totalSize for each site from the site uses stream
        Map<Integer, Long> total_size_per_site = siteUses.stream().collect(
                Collectors.groupingBy(siteUses1 -> siteUses1.getSite().getId(), Collectors.summingLong(SiteUses::getSize_sqft)));

        //find the primary type by finding the max size_sqft per site using the site uses Stream
        Map<Integer, Optional<SiteUses>> max_lot_size_per_site = siteUses.stream().collect(
                Collectors.groupingBy(siteUses1 -> siteUses1.getSite().getId(), Collectors.maxBy(Comparator.comparing(SiteUses::getSize_sqft))));
        SiteUses siteUsage = max_lot_size_per_site.get(id).orElseGet(SiteUses::new);
        //get the use_type from the use_types table
        UseTypes useTypes = useTypesRepository.findById(siteUsage.getUseTypes().getId()).orElseGet(UseTypes::new);

        return new MaxSqftSizeSiteData(site.getId(), site.getName(), site.getAddress(), site.getCity(), site.getState(), site.getZipcode(), total_size_per_site.get(id), useTypes);

    }

    /* Get all sites  */
    public List<Site> findAllSites() {

        List<Site> sites = (List<Site>) siteRepository.findAll();
        return sites;
    }

    /* Optional Task: Finding the Sites for a Particular type of Use */
    public Set<Site> findAllSitesForAUseType(Integer useTypeId) {

        Set<Site> sites = new HashSet<Site>();

        List<SiteUses> siteUses = siteUsesRepository.findAll();

        siteUses.stream()
                .filter(siteUses1 -> siteUses1.getUseTypes().getId() == useTypeId)
                .forEach(siteUses1 -> sites.add(siteUses1.getSite()));
        return sites;
    }

    /* Create/Update the site */
    public Site saveSite(Site site) {
        return siteRepository.save(site);
    }

    /* delete site by id  */
    public void deleteSiteById(Integer siteId) {
        siteRepository.deleteById(siteId);
    }

}


////////////////////////////////////////////////////////////
// Copyright 2018  Measurabl, Inc. All rights reserved.
////////////////////////////////////////////////////////////
    