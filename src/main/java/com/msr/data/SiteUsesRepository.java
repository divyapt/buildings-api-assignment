package com.msr.data;

import com.msr.model.SiteUses;
import com.msr.model.UseTypes;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Repository functionality for the SiteUses
 *
 */
public interface SiteUsesRepository extends JpaRepository<SiteUses,Integer> {

}
