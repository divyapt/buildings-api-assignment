package com.msr.model;

import com.msr.model.UseTypes;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MaxSqftSizeSiteData {

    private int id;

    private String name;

    private String address;

    private String city;

    private String state;

    private String zipcode;

    private long total_size;

    private UseTypes primary_type;

}
