package com.msr.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

/**
 * Site uses POJO
 *
 * @author Measurabl
 * @since 2019-06-11
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder (toBuilder = true)
@Entity
public class SiteUses {
    @Id
    private int id;

    private String description;

    private long size_sqft;


    @ManyToOne
    @JoinColumn(name = "useTypeId", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private UseTypes useTypes;

    @ManyToOne
    @JoinColumn(name = "siteId", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Site site;

}

////////////////////////////////////////////////////////////
// Copyright 2018  Measurabl, Inc. All rights reserved.
////////////////////////////////////////////////////////////
    