package com.msr.repository;

import com.msr.BuildingsApiApplication;
import com.msr.SiteServiceCommandLineRunner;
import com.msr.data.*;
import com.msr.exception.SiteNotFoundException;
import com.msr.model.MaxSqftSizeSiteData;
import com.msr.model.Site;
import com.msr.model.SiteUses;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Set;

//TODO: Just added a few simple unit tests for testing repository, Need to add more negative and positive tests
//TODO: Add tests for Rest Controller

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {BuildingsApiApplication.class, SiteServiceCommandLineRunner.class})
public class SiteDaoTest {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    SiteDao siteDao;

    @Autowired
    SiteRepository siteRepository;

    @Autowired
    SiteUsesRepository siteUsesRepository;

    @Autowired
    UseTypesRepository useTypesRepository;

    @Test
    public void findSiteById() {
        Site site = siteDao.findSiteById(5);
        assertEquals("Bellagio", site.getName());
    }

    @Test(expected = SiteNotFoundException.class)
    public void findNonExistingSite() {
        siteDao.findSiteById(9);
    }

    @Test
    @Transactional
    public void findMaxSizeSite() {
        MaxSqftSizeSiteData site = siteDao.findMaxSqftSizeSite(1);
        assertEquals(13000, site.getTotal_size());
        assertEquals("Data Center", site.getPrimary_type().getName());

        //modify the  size of a site use and check the total size
        SiteUses siteUse = siteDao.findSiteById(1).getSiteUses().get(0);
        siteUse = siteUse.toBuilder().size_sqft(4000).build();
        //SiteUses siteUseCopy = new SiteUses.S
        //SiteUses siteUseCopy = new SiteUses(siteUse.getId(),siteUse.getDescription(),4000, siteUse.getUseTypes(),siteUse.getSite());
        siteUsesRepository.save(siteUse);
        MaxSqftSizeSiteData site2 = siteDao.findMaxSqftSizeSite(1);
        assertEquals(14000, site2.getTotal_size());
    }

    @Test
    public void findAllSites() {
        List<Site> sites = siteDao.findAllSites();
        assertEquals(6, sites.size());
    }

    @Test
    public void findAllSitesForAUseType() {
        Set<Site> sites = siteDao.findAllSitesForAUseType(54);
        assertEquals(1, sites.size());
    }

    @Test
    public void findAllSitesForANonExistingUseType() {
        Set<Site> sites = siteDao.findAllSitesForAUseType(55);
        assertEquals(0, sites.size());
    }
}

